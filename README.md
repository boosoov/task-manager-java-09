# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: BORUAK SERGEY

**E-MAIL**: boosoov@gmail.com

# HARDWARE

**CPU**: Intel Core i3

**RAM**: 2 GB

**ROM**: 1 GB

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM BUILD 

```bash
mvn clean package
```

# PROGRAM RUN 

```bash
java -jar ./target/task-manager.jar
```