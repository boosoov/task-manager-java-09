package com.rencredit.jschool.boruak.taskmanager.api;

import com.rencredit.jschool.boruak.taskmanager.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
