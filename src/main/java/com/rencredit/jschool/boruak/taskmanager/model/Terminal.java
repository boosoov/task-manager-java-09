package com.rencredit.jschool.boruak.taskmanager.model;

import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;
import com.rencredit.jschool.boruak.taskmanager.repository.CommandRepository;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import com.rencredit.jschool.boruak.taskmanager.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Terminal {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void processCommand() {
        final Scanner scanner = new Scanner(System.in);
        String[] commands;
        while (true) {
            System.out.print("Enter command: ");
            commands = scanner.nextLine().split("\\s+");
            System.out.println();
            Terminal.parseArgs(commands);
        }
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        for (String arg : args) {
            processArg(TerminalCommandUtil.convertArgumentToCommand(arg));
            System.out.println();
        }
    }

    private static void processArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                System.exit((0));
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            default:
                showUnknownCommand(arg);
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Boruak Sergey");
        System.out.println("E-MAIL: boosoov@gmail.com");
    }

    private static void showCommands() {
        System.out.println("[COMMANDS]");
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    private static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final String[] arguments = COMMAND_REPOSITORY.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    private static void showUnknownCommand(final String arg) {
        System.out.println("Unknown command: " + arg);
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
